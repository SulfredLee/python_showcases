
#!/bin/bash

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

# start your server
cd "$SCRIPT_DIR"
cd ../python_showcases/app/grpc_api/
poetry run python python_showcases_grpc_server.py
# poetry run python python_showcases_grpc_server.py &
cd -

# start your next server
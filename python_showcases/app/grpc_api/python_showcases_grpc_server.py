
from concurrent import futures
import time
import os
import grpc

import sfdevtools.grpc_protos.python_showcases_pb2 as python_showcases_pb2
import sfdevtools.grpc_protos.python_showcases_pb2_grpc as python_showcases_pb2_grpc
import sfdevtools.observability.log_helper as lh

from python_showcases.app.grpc_api.main_manager import MainManager as mm

class Python_showcasesServicer(python_showcases_pb2_grpc.Python_showcasesServicer):
    def HealthCheck(self, request, context):
        logger = mm.instance().get_logger()

        logger.info(f"Healthcheck Request Made: {request.message}")
        pong = python_showcases_pb2.Pong()
        pong.message = f"Pong from grpc server"

        return pong

def serve():
    logger: logging.Logger = lh.init_logger(logger_name="python_showcases_grpc_server_logger", is_json_output=False)

    # get env variables
    env_v = {"host_name": os.getenv("GRPC_RUN_HOST", default="0.0.0.0")
             , "port": os.getenv("GRPC_RUN_PORT", default="50051")}
    logger.info(f"We get environment variables: {env_v}")

    main_m = mm.instance()
    main_m.init_component(logger=logger)

    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
    python_showcases_pb2_grpc.add_Python_showcasesServicer_to_server(Python_showcasesServicer(), server)
    server.add_insecure_port(f'{env_v["host_name"]}:{env_v["port"]}')
    server.start()
    server.wait_for_termination()

if __name__ == "__main__":
    serve()

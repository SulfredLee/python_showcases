
## python_showcases

### Project strucutre
![project structure](out/diagrams/project_structure/project_structure.png "Project structure")

- chart
  - This is a folder containing deployment information. The deployment tool is k8s.
- dockerEnv
  - This is a folder for docker container perparation.
  - We have development and running enviroment. They are kept in different dockerfiles.
- dockerEnv/dev
  - There is a script to manage the dev container under this folder
- python_showcases
  - There are 2 subdirectory. One is used to store application and the other is used to store libraries.
- scripts
  - That is used for starting servers withint the docker container. It is useful when we deploy our application to remote servers.
- tests
  - All the test cases should be saved here. We already included the testing phase in the CI/CD pipeline supported by gitlab.
- Doxyfile
  - This is used for documentation generation
- diagrams
  - Here we store puml files for diagrams generation
- poetry.lock, pyproject.toml
  - We use poetry as a package manager.

### How to publish to pypi
```bash
# set up pypi token
$ poetry config pypi-token.pypi my-token

# build the project
$ poetry build

# publish the project
$ poetry publish

# DONE
```

### How to use QC CLI
[reference](https://www.quantconnect.com/docs/v2/lean-cli)
```bash
# Install lean
$ poetry add lean
$ poetry install

# Login
$ poetry run lean login --user-id xxx --api-token xxx

# Initial workspace
$ poetry run lean init

# Pull projects
$ poetry run lean cloud pull

# Push projects
$ poetry run lean cloud push
```

### kubernetes helm related
```bash
# check helm template
$ helm template python-showcases ./chart --values=./chart/values.dev.yaml

# install helm chart
$ helm upgrade --wait --timeout=1200s --install --values ./chart/values.dev.yaml python-showcases ./chart

# uninstall helm chart
$ helm uninstall python-showcases
```

### GRPC related
[reference](https://github.com/chelseafarley/PythonGrpc)
```bash
# generate python script from proto file
$ cd python_showcases/app/grpc_api
$ poetry run python -m grpc_tools.protoc -I protos --python_out=. --grpc_python_out=. protos/python_showcases.proto

$ poetry add grpcio-tools
